-- drop schema if exists cognition cascade;
-- create schema cognition;
-- set search_path to cognition,public;
create extension if not exists timescaledb cascade;
create extension if not exists intarray cascade;
create extension if not exists pg_stat_statements cascade;
-- load 'pg_hint_plan';